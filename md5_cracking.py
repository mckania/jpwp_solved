import hashlib
import time

with open('reverse/10-hashed-md5.txt') as f:
    credentials = [line.rstrip('\n') for line in f];
f.close()

usernames = []
hashed_passwords = []
restored_passwords = []

for each in credentials:
    usernames.append(each[:each.find(":")])  #writing usernames to array
    hashed_passwords.append(each[each.find(":")+1:]);


letters = 'abcdefghijklmnopqrstuvwxyz'

complete_list = []

start = time.time()

for current in range(5):
    a = [i for i in letters]
    for y in range(current):
        a = [x+i for i in letters for x in a]
    complete_list = complete_list+a

for hash in hashed_passwords:
    for each in complete_list:
        if hashlib.md5(each.encode()).hexdigest() == hash:
            restored_passwords.append(each)
            break
        else:
            continue
end = time.time()
print('czas wykonania ' + str(end-start))

with open('reverse/restored_passwords.txt', 'w') as f: #saving to file
    for item in restored_passwords:
        f.write("%s\n" % item)
f.close()