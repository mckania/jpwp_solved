import hashlib

with open('credentails.txt') as f:
    credentials = [line.rstrip('\n') for line in f];
f.close()

usernames = []
passwords = []
credentials_md5 = []

for each in credentials:
    usernames.append(each[:each.find(":")])  #writing usernames to array
    each = each[each.find(":")+1:]
    hashmd5 = hashlib.md5(each.encode())    #hashing each password
    passwords.append(hashmd5.hexdigest())

for index in range(len(usernames)):
    credentials_md5.append(usernames[index] + ":" + passwords[index])   #writing username:hash to file

with open('credentials_md5.txt', 'w') as f: #saving to file
    for item in credentials_md5:
        f.write("%s\n" % item)
f.close()
