import hashlib
import uuid


def hashPass(password, salt=None, iterations=10000):
    if not salt:
        salt = uuid.uuid4().hex

    salt = str.encode(salt)
    password = str.encode(password)

    return bytes.hex(hashlib.pbkdf2_hmac('sha256', password, salt, 10000)) + ':' + salt.decode() + ':' + str(iterations)


def findPassword(username):
    hash, salt, iterations = 0, 0, 0
    for each in credentials_hashed:

        if username == each[:each.find(":")]:
            hash = each[each.find(":") + 1: each.find(":", each.find(":") + 1)]
            salt = each[each.find(":", each.find(":") + 1) + 1:each.rfind(":")]
            iterations = each[each.rfind(":") + 1:]
            break

    return hash, salt, iterations


def checkPassword(username, password):
    hash, salt, iterations = findPassword(username)

    if hashPass(password, salt, iterations) == hash + ":" + salt + ":" + iterations:
        print("valid password")
    else:
        print("password not valid")


###############################################################


with open('credentials.txt') as f:
    credentials = [line.rstrip('\n') for line in f]
f.close()

usernames = []
passwords = []
credentials_hashed = []

for each in credentials:
    usernames.append(each[:each.find(":")])  # writing usernames to array
    each = each[each.find(":") + 1:]
    hashedPass = hashPass(each)
    passwords.append(hashedPass)

for index in range(len(usernames) - 1):
    credentials_hashed.append(usernames[index] + ":" + passwords[index])  # writing username:hash to file

with open('credentials_pbkdf2.txt', 'w') as f:  # saving to file
    for item in credentials_hashed:
        f.write("%s\n" % item)
f.close()

if __name__ == "__main__":
    checkPassword('adoads', 'qwerty1')
    checkPassword('adoads', 'qwerty2')
