import uuid
import hashlib


def hashPass(text):
    salt = uuid.uuid4().hex
    return hashlib.sha512(salt.encode() + text.encode()).hexdigest() + ':' + salt


with open('credentials.txt') as f:
    credentials = [line.rstrip('\n') for line in f];
f.close()

usernames = []
passwords = []
credentials_sha = []

for each in credentials:
    usernames.append(each[:each.find(":")])  #writing usernames to array
    each = each[each.find(":")+1:]
    hashedPass = hashPass(each)
    passwords.append(hashedPass)

for index in range(len(usernames)-1):
    credentials_sha.append(usernames[index] + ":" + passwords[index])   #writing username:hash to file

with open('credentials_sha.txt', 'w') as f: #saving to file
    for item in credentials_sha:
        f.write("%s\n" % item)
f.close()






